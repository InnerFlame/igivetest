<?php
require_once("inc/init.inc.php");
$page_title = $lngstr['page_title_register'];
if(getConfigItem(CONFIG_can_register)) { 
	if(isset($_POST['bsubmit'])) {
 include_once($DOCUMENT_PAGES."register-2.inc.php");
} else {
 include_once($DOCUMENT_PAGES."register-1.inc.php");
}
} else { 
	$page_title = $lngstr['page_title_signin'];
$input_err_msg = $lngstr['err_no_permissions_to_register'];
include_once($DOCUMENT_PAGES."signin-1.inc.php");
}
?>

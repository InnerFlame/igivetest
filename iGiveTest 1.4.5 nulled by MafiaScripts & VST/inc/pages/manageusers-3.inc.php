<?php
$f_userid = (int)readGetVar('userid');
 
$f_user_enabled = (int)(boolean)readPostVar('user_enabled');
$qry_str = "UPDATE ".$srv_settings['table_prefix']."users SET user_enabled=".$f_user_enabled;
$f_user_expiredate_donotuse = (boolean)readPostVar('user_expiredate_donotuse');
if($f_user_expiredate_donotuse) {
	$f_user_expiredate = 0;
} else {
	$f_user_expiredate_year = (int)readPostVar('user_expiredate_year');
$f_user_expiredate_month = (int)readPostVar('user_expiredate_month');
$f_user_expiredate_day = (int)readPostVar('user_expiredate_day');
$f_user_expiredate_hour = (int)readPostVar('user_expiredate_hour');
$f_user_expiredate_minute = (int)readPostVar('user_expiredate_minute');
$f_user_expiredate = mktime($f_user_expiredate_hour, $f_user_expiredate_minute, 0, $f_user_expiredate_month, $f_user_expiredate_day, $f_user_expiredate_year);
}
$qry_str .= ", user_expiredate=".$f_user_expiredate;
$f_user_name = readPostVar('user_name');
$f_user_name = $g_db->qstr($f_user_name, get_magic_quotes_gpc());
$qry_str .= ", user_name=".$f_user_name;
$f_user_password = readPostVar('user_password');
if(get_magic_quotes_gpc())
 $i_pass_hash = stripslashes($f_user_password);
$i_pass_hash = md5($f_user_password);
$i_pass_hash = $g_db->qstr($i_pass_hash, get_magic_quotes_gpc());
$f_user_password_confirm = readPostVar('user_password_confirm'); 
if($f_user_password) {
	if($f_user_password == $f_user_password_confirm) {
 $qry_str .= ", user_passhash=".$i_pass_hash;
} else {
 $input_err_msg .= $lngstr['err_no_password'];
}
}
$f_user_email = readPostVar('user_email');
$f_user_email = $g_db->qstr($f_user_email, get_magic_quotes_gpc());
$qry_str .= ", user_email=".$f_user_email;
$f_user_firstname = readPostVar('user_firstname');
$f_user_firstname = $g_db->qstr($f_user_firstname, get_magic_quotes_gpc());
$qry_str .= ", user_firstname=".$f_user_firstname;
$f_user_lastname = readPostVar('user_lastname');
$f_user_lastname = $g_db->qstr($f_user_lastname, get_magic_quotes_gpc());
$qry_str .= ", user_lastname=".$f_user_lastname;
$f_user_middlename = readPostVar('user_middlename');
$f_user_middlename = $g_db->qstr($f_user_middlename, get_magic_quotes_gpc());
$qry_str .= ", user_middlename=".$f_user_middlename;
$f_user_address = readPostVar('user_address');
$f_user_address = $g_db->qstr($f_user_address, get_magic_quotes_gpc());
$qry_str .= ", user_address=".$f_user_address;
$f_user_city = readPostVar('user_city');
$f_user_city = $g_db->qstr($f_user_city, get_magic_quotes_gpc());
$qry_str .= ", user_city=".$f_user_city;
$f_user_state = readPostVar('user_state');
$f_user_state = $g_db->qstr($f_user_state, get_magic_quotes_gpc());
$qry_str .= ", user_state=".$f_user_state;
$f_user_zip = readPostVar('user_zip');
$f_user_zip = $g_db->qstr($f_user_zip, get_magic_quotes_gpc());
$qry_str .= ", user_zip=".$f_user_zip;
$f_user_country = readPostVar('user_country');
$f_user_country = $g_db->qstr($f_user_country, get_magic_quotes_gpc());
$qry_str .= ", user_country=".$f_user_country;
$f_user_phone = readPostVar('user_phone');
$f_user_phone = $g_db->qstr($f_user_phone, get_magic_quotes_gpc());
$qry_str .= ", user_phone=".$f_user_phone;
$f_user_fax = readPostVar('user_fax');
$f_user_fax = $g_db->qstr($f_user_fax, get_magic_quotes_gpc());
$qry_str .= ", user_fax=".$f_user_fax;
$f_user_mobile = readPostVar('user_mobile');
$f_user_mobile = $g_db->qstr($f_user_mobile, get_magic_quotes_gpc());
$qry_str .= ", user_mobile=".$f_user_mobile;
$f_user_pager = readPostVar('user_pager');
$f_user_pager = $g_db->qstr($f_user_pager, get_magic_quotes_gpc());
$qry_str .= ", user_pager=".$f_user_pager;
$f_user_ipphone = readPostVar('user_ipphone');
$f_user_ipphone = $g_db->qstr($f_user_ipphone, get_magic_quotes_gpc());
$qry_str .= ", user_ipphone=".$f_user_ipphone;
$f_user_webpage = readPostVar('user_webpage');
$f_user_webpage = $g_db->qstr($f_user_webpage, get_magic_quotes_gpc());
$qry_str .= ", user_webpage=".$f_user_webpage;
$f_user_icq = readPostVar('user_icq');
$f_user_icq = $g_db->qstr($f_user_icq, get_magic_quotes_gpc());
$qry_str .= ", user_icq=".$f_user_icq;
$f_user_msn = readPostVar('user_msn');
$f_user_msn = $g_db->qstr($f_user_msn, get_magic_quotes_gpc());
$qry_str .= ", user_msn=".$f_user_msn;
$f_user_aol = readPostVar('user_aol');
$f_user_aol = $g_db->qstr($f_user_aol, get_magic_quotes_gpc());
$qry_str .= ", user_aol=".$f_user_aol;
$f_user_gender = readPostVar('user_gender');
$f_user_gender = $g_db->qstr($f_user_gender, get_magic_quotes_gpc());
$qry_str .= ", user_gender=".$f_user_gender;
$f_user_birthday = readPostVar('user_birthday');
$f_user_birthday = $g_db->qstr($f_user_birthday, get_magic_quotes_gpc());
$qry_str .= ", user_birthday=".$f_user_birthday;
$f_user_husbandwife = readPostVar('user_husbandwife');
$f_user_husbandwife = $g_db->qstr($f_user_husbandwife, get_magic_quotes_gpc());
$qry_str .= ", user_husbandwife=".$f_user_husbandwife;
$f_user_children = readPostVar('user_children');
$f_user_children = $g_db->qstr($f_user_children, get_magic_quotes_gpc());
$qry_str .= ", user_children=".$f_user_children;
$f_user_trainer = readPostVar('user_trainer');
$f_user_trainer = $g_db->qstr($f_user_trainer, get_magic_quotes_gpc());
$qry_str .= ", user_trainer=".$f_user_trainer;
$f_user_photo = readPostVar('user_photo');
$f_user_photo = $g_db->qstr($f_user_photo, get_magic_quotes_gpc());
$qry_str .= ", user_photo=".$f_user_photo;
$f_user_company = readPostVar('user_company');
$f_user_company = $g_db->qstr($f_user_company, get_magic_quotes_gpc());
$qry_str .= ", user_company=".$f_user_company;
$f_user_cposition = readPostVar('user_cposition');
$f_user_cposition = $g_db->qstr($f_user_cposition, get_magic_quotes_gpc());
$qry_str .= ", user_cposition=".$f_user_cposition;
$f_user_department = readPostVar('user_department');
$f_user_department = $g_db->qstr($f_user_department, get_magic_quotes_gpc());
$qry_str .= ", user_department=".$f_user_department;
$f_user_coffice = readPostVar('user_coffice');
$f_user_coffice = $g_db->qstr($f_user_coffice, get_magic_quotes_gpc());
$qry_str .= ", user_coffice=".$f_user_coffice;
$f_user_caddress = readPostVar('user_caddress');
$f_user_caddress = $g_db->qstr($f_user_caddress, get_magic_quotes_gpc());
$qry_str .= ", user_caddress=".$f_user_caddress;
$f_user_ccity = readPostVar('user_ccity');
$f_user_ccity = $g_db->qstr($f_user_ccity, get_magic_quotes_gpc());
$qry_str .= ", user_ccity=".$f_user_ccity;
$f_user_cstate = readPostVar('user_cstate');
$f_user_cstate = $g_db->qstr($f_user_cstate, get_magic_quotes_gpc());
$qry_str .= ", user_cstate=".$f_user_cstate;
$f_user_czip = readPostVar('user_czip');
$f_user_czip = $g_db->qstr($f_user_czip, get_magic_quotes_gpc());
$qry_str .= ", user_czip=".$f_user_czip;
$f_user_ccountry = readPostVar('user_ccountry');
$f_user_ccountry = $g_db->qstr($f_user_ccountry, get_magic_quotes_gpc());
$qry_str .= ", user_ccountry=".$f_user_ccountry;
$f_user_cphone = readPostVar('user_cphone');
$f_user_cphone = $g_db->qstr($f_user_cphone, get_magic_quotes_gpc());
$qry_str .= ", user_cphone=".$f_user_cphone;
$f_user_cfax = readPostVar('user_cfax');
$f_user_cfax = $g_db->qstr($f_user_cfax, get_magic_quotes_gpc());
$qry_str .= ", user_cfax=".$f_user_cfax;
$f_user_cmobile = readPostVar('user_cmobile');
$f_user_cmobile = $g_db->qstr($f_user_cmobile, get_magic_quotes_gpc());
$qry_str .= ", user_cmobile=".$f_user_cmobile;
$f_user_cpager = readPostVar('user_cpager');
$f_user_cpager = $g_db->qstr($f_user_cpager, get_magic_quotes_gpc());
$qry_str .= ", user_cpager=".$f_user_cpager;
$f_user_cipphone = readPostVar('user_cipphone');
$f_user_cipphone = $g_db->qstr($f_user_cipphone, get_magic_quotes_gpc());
$qry_str .= ", user_cipphone=".$f_user_cipphone;
$f_user_cwebpage = readPostVar('user_cwebpage');
$f_user_cwebpage = $g_db->qstr($f_user_cwebpage, get_magic_quotes_gpc());
$qry_str .= ", user_cwebpage=".$f_user_cwebpage;
$f_user_cphoto = readPostVar('user_cphoto');
$f_user_cphoto = $g_db->qstr($f_user_cphoto, get_magic_quotes_gpc());
$qry_str .= ", user_cphoto=".$f_user_cphoto;
if(getConfigItem(CONFIG_reg_userfield1) > CONFIG_CONST_donotshow) {
	$f_user_ufield1 = readPostVar('user_ufield1');
$f_user_ufield1 = $g_db->qstr($f_user_ufield1, get_magic_quotes_gpc());
$qry_str .= ", user_ufield1=".$f_user_ufield1;
}
if(getConfigItem(CONFIG_reg_userfield2) > CONFIG_CONST_donotshow) {
	$f_user_ufield2 = readPostVar('user_ufield2');
$f_user_ufield2 = $g_db->qstr($f_user_ufield2, get_magic_quotes_gpc());
$qry_str .= ", user_ufield2=".$f_user_ufield2;
}
if(getConfigItem(CONFIG_reg_userfield3) > CONFIG_CONST_donotshow) {
	$f_user_ufield3 = readPostVar('user_ufield3');
$f_user_ufield3 = $g_db->qstr($f_user_ufield3, get_magic_quotes_gpc());
$qry_str .= ", user_ufield3=".$f_user_ufield3;
}
if(getConfigItem(CONFIG_reg_userfield4) > CONFIG_CONST_donotshow) {
	$f_user_ufield4 = readPostVar('user_ufield4');
$f_user_ufield4 = $g_db->qstr($f_user_ufield4, get_magic_quotes_gpc());
$qry_str .= ", user_ufield4=".$f_user_ufield4;
}
if(getConfigItem(CONFIG_reg_userfield5) > CONFIG_CONST_donotshow) {
	$f_user_ufield5 = readPostVar('user_ufield5');
$f_user_ufield5 = $g_db->qstr($f_user_ufield5, get_magic_quotes_gpc());
$qry_str .= ", user_ufield5=".$f_user_ufield5;
}
if(getConfigItem(CONFIG_reg_userfield6) > CONFIG_CONST_donotshow) {
	$f_user_ufield6 = readPostVar('user_ufield6');
$f_user_ufield6 = $g_db->qstr($f_user_ufield6, get_magic_quotes_gpc());
$qry_str .= ", user_ufield6=".$f_user_ufield6;
}
if(getConfigItem(CONFIG_reg_userfield7) > CONFIG_CONST_donotshow) {
	$f_user_ufield7 = readPostVar('user_ufield7');
$f_user_ufield7 = $g_db->qstr($f_user_ufield7, get_magic_quotes_gpc());
$qry_str .= ", user_ufield7=".$f_user_ufield7;
}
if(getConfigItem(CONFIG_reg_userfield8) > CONFIG_CONST_donotshow) {
	$f_user_ufield8 = readPostVar('user_ufield8');
$f_user_ufield8 = $g_db->qstr($f_user_ufield8, get_magic_quotes_gpc());
$qry_str .= ", user_ufield8=".$f_user_ufield8;
}
if(getConfigItem(CONFIG_reg_userfield9) > CONFIG_CONST_donotshow) {
	$f_user_ufield9 = readPostVar('user_ufield9');
$f_user_ufield9 = $g_db->qstr($f_user_ufield9, get_magic_quotes_gpc());
$qry_str .= ", user_ufield9=".$f_user_ufield9;
}
if(getConfigItem(CONFIG_reg_userfield10) > CONFIG_CONST_donotshow) {
	$f_user_ufield10 = readPostVar('user_ufield10');
$f_user_ufield10 = $g_db->qstr($f_user_ufield10, get_magic_quotes_gpc());
$qry_str .= ", user_ufield10=".$f_user_ufield10;
}
$f_user_notes = readPostVar('user_notes');
$f_user_notes = $g_db->qstr($f_user_notes, get_magic_quotes_gpc());
$qry_str .= ", user_notes=$f_user_notes";
$qry_str .= " WHERE userid=$f_userid";
 
if($i_rSet1 = $g_db->Execute("SELECT user_name FROM ".$srv_settings['table_prefix']."users WHERE user_name=$f_user_name AND userid<>$f_userid")) {
	$sql_username_duplicates = $i_rSet1->RecordCount() > 0;
$i_rSet1->Close();
} else {
 $sql_username_duplicates = false;
}
if($sql_username_duplicates)
 $input_err_msg .= $lngstr['err_username_duplicate'];
 
if($input_err_msg) {
	include_once($DOCUMENT_PAGES."manageusers-2.inc.php");
} else { 
	if($g_db->Execute($qry_str)===false)
 showDBError(__FILE__, 2);
gotoLocation('users.php');
}
?>

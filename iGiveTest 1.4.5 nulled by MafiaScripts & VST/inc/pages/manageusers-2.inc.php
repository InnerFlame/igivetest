<?php
require_once($DOCUMENT_INC."top.inc.php");
$f_userid = (int)readGetVar('userid');
$i_items = array();
array_push($i_items, array(0 => '<a class=bar2 href="users.php">'.$lngstr['page_header_manageusers'].'</a>', 0));
array_push($i_items, array(0 => '<a class=bar2 href="groups.php">'.$lngstr['page_header_managegroups'].'</a>', 0));
array_push($i_items, array(0 => '<a class=bar2 href="config.php">'.$lngstr['page_header_config'].'</a>', 0));
array_push($i_items, array(0 => '<a class=bar2 href="email-templates.php">'.$lngstr['page_header_emailtemplates'].'</a>', 0));
array_push($i_items, array(0 => '<a class=bar2 href="report-templates.php">'.$lngstr['page_header_rtemplates'].'</a>', 0));
array_push($i_items, array(0 => '<a class=bar2 href="visitors.php">'.$lngstr['page_header_visitors'].'</a>', 0));
writePanel2($i_items);
echo '<h2>'.$lngstr['page_header_users_settings'].'</h2>';
writeErrorsWarningsBar();
 
$i_rSet1 = $g_db->Execute("SELECT * FROM ".$srv_settings['table_prefix']."users WHERE userid=$f_userid");
if(!$i_rSet1) {
	showDBError(__FILE__, 1);
} else {
	if(!$i_rSet1->EOF) {
 echo '<p><form method=post action="users.php?userid='.$f_userid.'&action=edit">';
echo '<table class=rowtable2 cellpadding=5 cellspacing=1 border=0 width="100%">';
echo '<tr class=rowtwo valign=top><td colspan=2>'.getCheckbox('user_enabled', $i_rSet1->fields["user_enabled"], $lngstr['page-users']['userenabled']).'</td></tr>';
$i_rowno = 1;
writeTR2Fixed($lngstr['page-users']['joindate'], date($lngstr['date_format_full'], $i_rSet1->fields["user_joindate"]));
writeTR2Fixed($lngstr['page-users']['logindate'], date($lngstr['date_format_full'], $i_rSet1->fields["user_logindate"]));
writeTR2Fixed($lngstr['page-users']['expiredate'], getCalendar("user_expiredate", date('Y', $i_rSet1->fields["user_expiredate"]), date('m', $i_rSet1->fields["user_expiredate"]), date('d', $i_rSet1->fields["user_expiredate"]), date('H', $i_rSet1->fields["user_expiredate"]), date('i', $i_rSet1->fields["user_expiredate"]), true));
writeTR2Fixed($lngstr['page-users']['username'], getInputElement('user_name', $i_rSet1->fields["user_name"]));
writeTR2Fixed($lngstr['page-users']['password_new'], getPasswordBox('user_password', ''));
writeTR2Fixed($lngstr['page-users']['password_confirm'], getPasswordBox('user_password_confirm', ''));
writeTR2Fixed($lngstr['page-users']['email'], getInputElement('user_email', $i_rSet1->fields["user_email"]));
writeTR2Fixed($lngstr['page-users']['firstname'], getInputElement('user_firstname', $i_rSet1->fields["user_firstname"]));
writeTR2Fixed($lngstr['page-users']['lastname'], getInputElement('user_lastname', $i_rSet1->fields["user_lastname"]));
writeTR2Fixed($lngstr['page-users']['middlename'], getInputElement('user_middlename', $i_rSet1->fields["user_middlename"]));
echo '<tr valign=top><td class=rowhdr2 colspan=2><a class=rowhdr2 href="javascript:void(0)" onclick="javascript:toggleSection(\'div_users_personal\')">'.$lngstr['page-users']['section_personal'].'</td></tr>';
echo '<tr valign=top><td class=rowone colspan=2><div id=div_users_personal style="display:'.(isset($_COOKIE['div_users_personal']) && $_COOKIE['div_users_personal']=='Y' ? 'block' : 'none').'"><table class=rowtable2 cellpadding=5 cellspacing=1 border=0 width="100%">';
writeTR2Fixed($lngstr['page-users']['address'], getTextArea('user_address', $i_rSet1->fields["user_address"]));
writeTR2Fixed($lngstr['page-users']['city'], getInputElement('user_city', $i_rSet1->fields["user_city"]));
writeTR2Fixed($lngstr['page-users']['state'], getInputElement('user_state', $i_rSet1->fields["user_state"]));
writeTR2Fixed($lngstr['page-users']['zip'], getInputElement('user_zip', $i_rSet1->fields["user_zip"]));
writeTR2Fixed($lngstr['page-users']['country'], getInputElement('user_country', $i_rSet1->fields["user_country"]));
writeTR2Fixed($lngstr['page-users']['phone'], getInputElement('user_phone', $i_rSet1->fields["user_phone"]));
writeTR2Fixed($lngstr['page-users']['fax'], getInputElement('user_fax', $i_rSet1->fields["user_fax"]));
writeTR2Fixed($lngstr['page-users']['mobile'], getInputElement('user_mobile', $i_rSet1->fields["user_mobile"]));
writeTR2Fixed($lngstr['page-users']['pager'], getInputElement('user_pager', $i_rSet1->fields["user_pager"]));
writeTR2Fixed($lngstr['page-users']['ipphone'], getInputElement('user_ipphone', $i_rSet1->fields["user_ipphone"]));
writeTR2Fixed($lngstr['page-users']['webpage'], getInputElement('user_webpage', $i_rSet1->fields["user_webpage"]));
writeTR2Fixed($lngstr['page-users']['icq'], getInputElement('user_icq', $i_rSet1->fields["user_icq"]));
writeTR2Fixed($lngstr['page-users']['msn'], getInputElement('user_msn', $i_rSet1->fields["user_msn"]));
writeTR2Fixed($lngstr['page-users']['aol'], getInputElement('user_aol', $i_rSet1->fields["user_aol"]));
writeTR2Fixed($lngstr['page-users']['gender'], getSelectElement('user_gender', $i_rSet1->fields["user_gender"], $lngstr['label_gender_items'])); 
 writeTR2Fixed($lngstr['page-users']['husbandwife'], getInputElement('user_husbandwife', $i_rSet1->fields["user_husbandwife"]));
writeTR2Fixed($lngstr['page-users']['children'], getInputElement('user_children', $i_rSet1->fields["user_children"]));
writeTR2Fixed($lngstr['page-users']['trainer'], getInputElement('user_trainer', $i_rSet1->fields["user_trainer"])); 
 echo '</table></div></td></tr>';
echo '<tr valign=top><td class=rowhdr2 colspan=2><a class=rowhdr2 href="javascript:void(0)" onclick="javascript:toggleSection(\'div_users_work\')">'.$lngstr['page-users']['section_work'].'</td></tr>';
echo '<tr valign=top><td class=rowone colspan=2><div id=div_users_work style="display:'.(isset($_COOKIE['div_users_work']) && $_COOKIE['div_users_work']=='Y' ? 'block' : 'none').'"><table class=rowtable2 cellpadding=5 cellspacing=1 border=0 width="100%">';
writeTR2Fixed($lngstr['page-users']['company'], getInputElement('user_company', $i_rSet1->fields["user_company"]));
writeTR2Fixed($lngstr['page-users']['cposition'], getInputElement('user_cposition', $i_rSet1->fields["user_cposition"]));
writeTR2Fixed($lngstr['page-users']['department'], getInputElement('user_department', $i_rSet1->fields["user_department"]));
writeTR2Fixed($lngstr['page-users']['coffice'], getInputElement('user_coffice', $i_rSet1->fields["user_coffice"]));
writeTR2Fixed($lngstr['page-users']['caddress'], getTextArea('user_caddress', $i_rSet1->fields["user_caddress"]));
writeTR2Fixed($lngstr['page-users']['ccity'], getInputElement('user_ccity', $i_rSet1->fields["user_ccity"]));
writeTR2Fixed($lngstr['page-users']['cstate'], getInputElement('user_cstate', $i_rSet1->fields["user_cstate"]));
writeTR2Fixed($lngstr['page-users']['czip'], getInputElement('user_czip', $i_rSet1->fields["user_czip"]));
writeTR2Fixed($lngstr['page-users']['ccountry'], getInputElement('user_ccountry', $i_rSet1->fields["user_ccountry"]));
writeTR2Fixed($lngstr['page-users']['cphone'], getInputElement('user_cphone', $i_rSet1->fields["user_cphone"]));
writeTR2Fixed($lngstr['page-users']['cfax'], getInputElement('user_cfax', $i_rSet1->fields["user_cfax"]));
writeTR2Fixed($lngstr['page-users']['cmobile'], getInputElement('user_cmobile', $i_rSet1->fields["user_cmobile"]));
writeTR2Fixed($lngstr['page-users']['cpager'], getInputElement('user_cpager', $i_rSet1->fields["user_cpager"]));
writeTR2Fixed($lngstr['page-users']['cipphone'], getInputElement('user_cipphone', $i_rSet1->fields["user_cipphone"]));
writeTR2Fixed($lngstr['page-users']['cwebpage'], getInputElement('user_cwebpage', $i_rSet1->fields["user_cwebpage"])); 
 echo '</table></div></td></tr>';
echo '<tr valign=top><td class=rowhdr2 colspan=2><a class=rowhdr2 href="javascript:void(0)" onclick="javascript:toggleSection(\'div_users_additional\')">'.$lngstr['page-users']['section_additional'].'</td></tr>';
echo '<tr valign=top><td class=rowone colspan=2><div id=div_users_additional style="display:'.(isset($_COOKIE['div_users_additional']) && $_COOKIE['div_users_additional']=='Y' ? 'block' : 'none').'"><table class=rowtable2 cellpadding=5 cellspacing=1 border=0 width="100%">';
if(isset($lngstr['custom']['user_userfield1_items'])) {
 $i_userfield1_items = $lngstr['custom']['user_userfield1_items'];
} else {
 $i_userfield1_items = array();
$i_items = explode(',', getConfigItem(CONFIG_reg_values_userfield1));
foreach($i_items as $val)
 $i_userfield1_items[$val] = $val;
}
if(getConfigItem(CONFIG_reg_userfield1) > CONFIG_CONST_donotshow)
 writeTR2Fixed(getConfigItem(CONFIG_reg_caption_userfield1).':', (getConfigItem(CONFIG_reg_type_userfield1)==CONFIG_CONST_type_singlelinetext ? getInputElement('user_ufield1', $i_rSet1->fields["user_ufield1"]) : (getConfigItem(CONFIG_reg_type_userfield1)==CONFIG_CONST_type_multilinetext ? getTextArea('user_ufield1', $i_rSet1->fields["user_ufield1"]) : getSelectElement('user_ufield1', $i_rSet1->fields["user_ufield1"], $i_userfield1_items))));
if(isset($lngstr['custom']['user_userfield2_items'])) {
 $i_userfield2_items = $lngstr['custom']['user_userfield2_items'];
} else {
 $i_userfield2_items = array();
$i_items = explode(',', getConfigItem(CONFIG_reg_values_userfield2));
foreach($i_items as $val)
 $i_userfield2_items[$val] = $val;
}
if(getConfigItem(CONFIG_reg_userfield2) > CONFIG_CONST_donotshow)
 writeTR2Fixed(getConfigItem(CONFIG_reg_caption_userfield2).':', (getConfigItem(CONFIG_reg_type_userfield2)==CONFIG_CONST_type_singlelinetext ? getInputElement('user_ufield2', $i_rSet1->fields["user_ufield2"]) : (getConfigItem(CONFIG_reg_type_userfield2)==CONFIG_CONST_type_multilinetext ? getTextArea('user_ufield2', $i_rSet1->fields["user_ufield2"]) : getSelectElement('user_ufield2', $i_rSet1->fields["user_ufield2"], $i_userfield2_items))));
if(isset($lngstr['custom']['user_userfield3_items'])) {
 $i_userfield3_items = $lngstr['custom']['user_userfield3_items'];
} else {
 $i_userfield3_items = array();
$i_items = explode(',', getConfigItem(CONFIG_reg_values_userfield3));
foreach($i_items as $val)
 $i_userfield3_items[$val] = $val;
}
if(getConfigItem(CONFIG_reg_userfield3) > CONFIG_CONST_donotshow)
 writeTR2Fixed(getConfigItem(CONFIG_reg_caption_userfield3).':', (getConfigItem(CONFIG_reg_type_userfield3)==CONFIG_CONST_type_singlelinetext ? getInputElement('user_ufield3', $i_rSet1->fields["user_ufield3"]) : (getConfigItem(CONFIG_reg_type_userfield3)==CONFIG_CONST_type_multilinetext ? getTextArea('user_ufield3', $i_rSet1->fields["user_ufield3"]) : getSelectElement('user_ufield3', $i_rSet1->fields["user_ufield3"], $i_userfield3_items))));
if(isset($lngstr['custom']['user_userfield4_items'])) {
 $i_userfield4_items = $lngstr['custom']['user_userfield4_items'];
} else {
 $i_userfield4_items = array();
$i_items = explode(',', getConfigItem(CONFIG_reg_values_userfield4));
foreach($i_items as $val)
 $i_userfield4_items[$val] = $val;
}
if(getConfigItem(CONFIG_reg_userfield4) > CONFIG_CONST_donotshow)
 writeTR2Fixed(getConfigItem(CONFIG_reg_caption_userfield4).':', (getConfigItem(CONFIG_reg_type_userfield4)==CONFIG_CONST_type_singlelinetext ? getInputElement('user_ufield4', $i_rSet1->fields["user_ufield4"]) : (getConfigItem(CONFIG_reg_type_userfield4)==CONFIG_CONST_type_multilinetext ? getTextArea('user_ufield4', $i_rSet1->fields["user_ufield4"]) : getSelectElement('user_ufield4', $i_rSet1->fields["user_ufield4"], $i_userfield4_items))));
if(isset($lngstr['custom']['user_userfield5_items'])) {
 $i_userfield5_items = $lngstr['custom']['user_userfield5_items'];
} else {
 $i_userfield5_items = array();
$i_items = explode(',', getConfigItem(CONFIG_reg_values_userfield5));
foreach($i_items as $val)
 $i_userfield5_items[$val] = $val;
}
if(getConfigItem(CONFIG_reg_userfield5) > CONFIG_CONST_donotshow)
 writeTR2Fixed(getConfigItem(CONFIG_reg_caption_userfield5).':', (getConfigItem(CONFIG_reg_type_userfield5)==CONFIG_CONST_type_singlelinetext ? getInputElement('user_ufield5', $i_rSet1->fields["user_ufield5"]) : (getConfigItem(CONFIG_reg_type_userfield5)==CONFIG_CONST_type_multilinetext ? getTextArea('user_ufield5', $i_rSet1->fields["user_ufield5"]) : getSelectElement('user_ufield5', $i_rSet1->fields["user_ufield5"], $i_userfield5_items))));
if(isset($lngstr['custom']['user_userfield6_items'])) {
 $i_userfield6_items = $lngstr['custom']['user_userfield6_items'];
} else {
 $i_userfield6_items = array();
$i_items = explode(',', getConfigItem(CONFIG_reg_values_userfield6));
foreach($i_items as $val)
 $i_userfield6_items[$val] = $val;
}
if(getConfigItem(CONFIG_reg_userfield6) > CONFIG_CONST_donotshow)
 writeTR2Fixed(getConfigItem(CONFIG_reg_caption_userfield6).':', (getConfigItem(CONFIG_reg_type_userfield6)==CONFIG_CONST_type_singlelinetext ? getInputElement('user_ufield6', $i_rSet1->fields["user_ufield6"]) : (getConfigItem(CONFIG_reg_type_userfield6)==CONFIG_CONST_type_multilinetext ? getTextArea('user_ufield6', $i_rSet1->fields["user_ufield6"]) : getSelectElement('user_ufield6', $i_rSet1->fields["user_ufield6"], $i_userfield6_items))));
if(isset($lngstr['custom']['user_userfield7_items'])) {
 $i_userfield7_items = $lngstr['custom']['user_userfield7_items'];
} else {
 $i_userfield7_items = array();
$i_items = explode(',', getConfigItem(CONFIG_reg_values_userfield7));
foreach($i_items as $val)
 $i_userfield7_items[$val] = $val;
}
if(getConfigItem(CONFIG_reg_userfield7) > CONFIG_CONST_donotshow)
 writeTR2Fixed(getConfigItem(CONFIG_reg_caption_userfield7).':', (getConfigItem(CONFIG_reg_type_userfield7)==CONFIG_CONST_type_singlelinetext ? getInputElement('user_ufield7', $i_rSet1->fields["user_ufield7"]) : (getConfigItem(CONFIG_reg_type_userfield7)==CONFIG_CONST_type_multilinetext ? getTextArea('user_ufield7', $i_rSet1->fields["user_ufield7"]) : getSelectElement('user_ufield7', $i_rSet1->fields["user_ufield7"], $i_userfield7_items))));
if(isset($lngstr['custom']['user_userfield8_items'])) {
 $i_userfield8_items = $lngstr['custom']['user_userfield8_items'];
} else {
 $i_userfield8_items = array();
$i_items = explode(',', getConfigItem(CONFIG_reg_values_userfield8));
foreach($i_items as $val)
 $i_userfield8_items[$val] = $val;
}
if(getConfigItem(CONFIG_reg_userfield8) > CONFIG_CONST_donotshow)
 writeTR2Fixed(getConfigItem(CONFIG_reg_caption_userfield8).':', (getConfigItem(CONFIG_reg_type_userfield8)==CONFIG_CONST_type_singlelinetext ? getInputElement('user_ufield8', $i_rSet1->fields["user_ufield8"]) : (getConfigItem(CONFIG_reg_type_userfield8)==CONFIG_CONST_type_multilinetext ? getTextArea('user_ufield8', $i_rSet1->fields["user_ufield8"]) : getSelectElement('user_ufield8', $i_rSet1->fields["user_ufield8"], $i_userfield8_items))));
if(isset($lngstr['custom']['user_userfield9_items'])) {
 $i_userfield9_items = $lngstr['custom']['user_userfield9_items'];
} else {
 $i_userfield9_items = array();
$i_items = explode(',', getConfigItem(CONFIG_reg_values_userfield9));
foreach($i_items as $val)
 $i_userfield9_items[$val] = $val;
}
if(getConfigItem(CONFIG_reg_userfield9) > CONFIG_CONST_donotshow)
 writeTR2Fixed(getConfigItem(CONFIG_reg_caption_userfield9).':', (getConfigItem(CONFIG_reg_type_userfield9)==CONFIG_CONST_type_singlelinetext ? getInputElement('user_ufield9', $i_rSet1->fields["user_ufield9"]) : (getConfigItem(CONFIG_reg_type_userfield9)==CONFIG_CONST_type_multilinetext ? getTextArea('user_ufield9', $i_rSet1->fields["user_ufield9"]) : getSelectElement('user_ufield9', $i_rSet1->fields["user_ufield9"], $i_userfield9_items))));
if(isset($lngstr['custom']['user_userfield10_items'])) {
 $i_userfield10_items = $lngstr['custom']['user_userfield10_items'];
} else {
 $i_userfield10_items = array();
$i_items = explode(',', getConfigItem(CONFIG_reg_values_userfield10));
foreach($i_items as $val)
 $i_userfield10_items[$val] = $val;
}
if(getConfigItem(CONFIG_reg_userfield10) > CONFIG_CONST_donotshow)
 writeTR2Fixed(getConfigItem(CONFIG_reg_caption_userfield10).':', (getConfigItem(CONFIG_reg_type_userfield10)==CONFIG_CONST_type_singlelinetext ? getInputElement('user_ufield10', $i_rSet1->fields["user_ufield10"]) : (getConfigItem(CONFIG_reg_type_userfield10)==CONFIG_CONST_type_multilinetext ? getTextArea('user_ufield10', $i_rSet1->fields["user_ufield10"]) : getSelectElement('user_ufield10', $i_rSet1->fields["user_ufield10"], $i_userfield10_items))));
echo '</table></div></td></tr>';
echo '<tr valign=top><td class=rowhdr2 colspan=2><a class=rowhdr2 href="javascript:void(0)" onclick="javascript:toggleSection(\'div_users_notes\')">'.$lngstr['page-users']['section_notes'].'</td></tr>';
echo '<tr valign=top><td class=rowone colspan=2><div id=div_users_notes style="display:'.(isset($_COOKIE['div_users_notes']) && $_COOKIE['div_users_notes']=='Y' ? 'block' : 'none').'"><table class=rowtable2 cellpadding=5 cellspacing=1 border=0 width="100%">';
writeTR2Fixed($lngstr['page-users']['notes'], getTextArea('user_notes', $i_rSet1->fields["user_notes"]));
echo '</table></div></td></tr>';
echo '</table>'; 
 echo '<p class=center><input class=btn type=submit name=bsubmit value=" '.$lngstr['button_update'].' "> <input class=btn type=submit name=bcancel value=" '.$lngstr['button_cancel'].' "></form>';
}
$i_rSet1->Close();
}
require_once($DOCUMENT_INC."btm.inc.php");
?>

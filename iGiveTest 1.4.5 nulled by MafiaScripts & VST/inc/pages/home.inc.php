<?php
require_once($DOCUMENT_INC."top.inc.php");
echo '<h2>'.$lngstr['page_header_panel'].'</h2>';
writeErrorsWarningsBar();
 
echo '<p><table class=rowtable2 cellpadding=5 cellspacing=1 border=0 width="100%">';
echo '<tr><td class=rowhdr1 title="'.$lngstr['page_panel_hdr_test_hint'].'">'.$lngstr['page_panel_hdr_test'].'</td><td class=rowhdr1 title="'.$lngstr['page_panel_hdr_teststatus_hint'].'">'.$lngstr['page_panel_hdr_teststatus'].'</td><td class=rowhdr1 title="'.$lngstr['page_panel_hdr_gettest_hint'].'">'.$lngstr['page_panel_hdr_gettest'].'</td></tr>';
$now = time();
$i_test_count = 0;
if($G_SESSION['access_tests'] > 0) { 
	$i_rSet1 = $g_db->Execute("SELECT testid, test_name, test_description, test_datestart, test_attempts, test_forall FROM ".$srv_settings['table_prefix']."tests WHERE test_enabled=1 AND test_dateend>$now");
if(!$i_rSet1) {
 showDBError(__FILE__, 1);
} else {
 while(!$i_rSet1->EOF) { 
 $i_isallowed = $i_rSet1->fields["test_forall"];
if(!$i_isallowed)
 $i_isallowed = getRecordCount($srv_settings['table_prefix'].'groups_users, '.$srv_settings['table_prefix'].'groups_tests', "(".$srv_settings['table_prefix']."groups_tests.testid=".$i_rSet1->fields["testid"]." AND ".$srv_settings['table_prefix']."groups_tests.groupid=".$srv_settings['table_prefix']."groups_users.groupid AND ".$srv_settings['table_prefix']."groups_users.userid=".$G_SESSION['userid'].")") > 0; 
 $i_attempt_count = 0;
$i_rSet3 = $g_db->Execute("SELECT test_attempt_count FROM ".$srv_settings['table_prefix']."tests_attempts WHERE testid=".$i_rSet1->fields["testid"]." AND userid=".$G_SESSION['userid']);
if(!$i_rSet3) {
 showDBError(__FILE__, 3);
} else {
 if(!$i_rSet3->EOF)
 $i_attempt_count = $i_rSet3->fields["test_attempt_count"];
$i_rSet3->Close();
} 
 if($i_isallowed) {
 $i_rownotext = (++$i_test_count % 2) ? "rowtwo" : "rowone";
if($i_rSet1->fields["test_datestart"] >= $now) { 
 echo '<tr class='.$i_rownotext.'><td><b>'.($i_rSet1->fields["test_name"] ? convertTextValue($i_rSet1->fields["test_name"]) : $lngstr['label_noname']).'</b><br>'.convertTextValue($i_rSet1->fields["test_description"]).'</td><td align=center>'.sprintf($lngstr['page_panel_status_will_be_available_on'], date($lngstr['date_format'], $i_rSet1->fields["test_datestart"])).'</td><td align=center><b>'.$lngstr['page_panel_get_test_link'].'</b></td></tr>';
} else if ($i_rSet1->fields["test_attempts"] > 0) {  
 if($i_attempt_count >= $i_rSet1->fields["test_attempts"]) {
 echo '<tr class='.$i_rownotext.'><td><b>'.($i_rSet1->fields["test_name"] ? convertTextValue($i_rSet1->fields["test_name"]) : $lngstr['label_noname']).'</b><br>'.convertTextValue($i_rSet1->fields["test_description"]).'</td><td align=center>'.$lngstr['page-takeatest']['attempts_limit_reached'].'</td><td align=center><b>'.$lngstr['page_panel_get_test_link'].'</b></td></tr>';
} else {
 echo '<tr class='.$i_rownotext.'><td width="60%"><b><a href="test.php?testid='.$i_rSet1->fields["testid"].'">'.($i_rSet1->fields["test_name"] ? convertTextValue($i_rSet1->fields["test_name"]) : $lngstr['label_noname']).'</a></b><br>'.convertTextValue($i_rSet1->fields["test_description"]).'</td><td align=center>'.sprintf($lngstr['page-takeatest']['attempts_left'], $i_rSet1->fields["test_attempts"] - $i_attempt_count).'</td><td align=center><b><a href="test.php?testid='.$i_rSet1->fields["testid"].'">'.$lngstr['page_panel_get_test_link'].'</a></b></td></tr>';
}
} else { 
 echo '<tr class='.$i_rownotext.'><td width="60%"><b><a href="test.php?testid='.$i_rSet1->fields["testid"].'">'.($i_rSet1->fields["test_name"] ? convertTextValue($i_rSet1->fields["test_name"]) : $lngstr['label_noname']).'</a></b><br>'.convertTextValue($i_rSet1->fields["test_description"]).'</td><td align=center>'.$lngstr['page_panel_status_available'].'</td><td align=center><b><a href="test.php?testid='.$i_rSet1->fields["testid"].'">'.$lngstr['page_panel_get_test_link'].'</a></b></td></tr>';
}
}
$i_rSet1->MoveNext();
}
$i_rSet1->Close();
}
} 
if(!$i_test_count)
 echo '<tr class=rowone><td colspan=3>'.$lngstr['err_no_tests'].'</td></tr>';
echo '</table>';
require_once($DOCUMENT_INC."btm.inc.php");
?>

<?php
require_once($DOCUMENT_INC."top.inc.php");
$i_items = array();
array_push($i_items, array(0 => $lngstr['page_header_results'], 1));
writePanel2($i_items);
echo '<h2>'.$lngstr['page_header_results'].'</h2>';
writeErrorsWarningsBar();
writeInfoBar($lngstr['tooltip_reportsmanager']);
$i_pagewide_id = 0; 
$i_userid_addon = "";
$i_sql_where_addon = "1=0 AND "; 
if($G_SESSION['access_reportsmanager'] > 1) {
	if(isset($_GET["userid"]) && $_GET["userid"] != "") {
 $i_userid_addon .= "&userid=".(int)$_GET["userid"];
$i_sql_where_addon = $srv_settings['table_prefix']."results.userid=".(int)$_GET["userid"]." AND ";
} else {
 $i_sql_where_addon = "";
}
} else {
	$i_sql_where_addon = $srv_settings['table_prefix']."results.userid=".$G_SESSION['userid']." AND ";
} 
$i_testid_addon = "";
if(isset($_GET["testid"]) && $_GET["testid"] != "") {
	$i_testid_addon .= "&testid=".(int)$_GET["testid"];
$i_sql_where_addon .= $srv_settings['table_prefix']."results.testid=".(int)$_GET["testid"]." AND ";
} 
$i_direction = "";
$i_order_addon = "";
$i_sql_order_addon = "";
$i_tablefields = array(
	array($lngstr["label_report_hdr_resultid"], $lngstr["label_report_hdr_resultid_hint"], $srv_settings['table_prefix']."results.resultid"),
	array($lngstr["label_report_hdr_result_datestart"], $lngstr["label_report_hdr_result_datestart_hint"], $srv_settings['table_prefix']."results.result_datestart"),
	array($lngstr["label_report_hdr_user_name"], $lngstr["label_report_hdr_user_name_hint"], $srv_settings['table_prefix']."users.user_name"),
	array($lngstr["label_report_hdr_test_name"], $lngstr["label_report_hdr_test_name_hint"], $srv_settings['table_prefix']."tests.test_name"),
	array($lngstr['page-report']['hdr_test_attempts'], $lngstr['page-report']['hdr_test_attempts_hint'], ""),
	array($lngstr["label_report_hdr_result_timeexceeded"], $lngstr["label_report_hdr_result_timeexceeded_hint"], $srv_settings['table_prefix']."results.result_timeexceeded"),
	array($lngstr["label_report_hdr_result_points"], $lngstr["label_report_hdr_result_points_hint"], $srv_settings['table_prefix']."results.result_points"),
	array($lngstr["label_report_hdr_result_pointsmax"], $lngstr["label_report_hdr_result_pointsmax_hint"], $srv_settings['table_prefix']."results.result_pointsmax"),
	array($lngstr["label_report_hdr_result_score"], $lngstr["label_report_hdr_result_score_hint"], "result_score"),
	array($lngstr["label_report_hdr_gscale_gradeid"], $lngstr["label_report_hdr_gscale_gradeid_hint"], $srv_settings['table_prefix']."gscales_grades.gscale_gradeid"),
);
$i_order_no = isset($_GET["order"]) ? (int)$_GET["order"] : 0;
if($i_order_no>=count($i_tablefields))
 $i_order_no = -1;
if($i_order_no>=0) {
	$i_direction = (isset($_GET["direction"]) && $_GET["direction"]) ? "DESC" : "";
$i_order_addon = "&order=".$i_order_no."&direction=".$i_direction;
$i_sql_order_addon = " ORDER BY ".$i_tablefields[$i_order_no][2]." ".$i_direction;
} 
$i_url_limitto_addon = "";
$i_url_pageno_addon = "";
$i_url_limit_addon = "";
$i_pageno = 0;
$i_limitcount = isset($_GET["limitto"]) ? (int)$_GET["limitto"] : $G_SESSION['config_itemsperpage'];
if($i_limitcount>0) {
	$i_recordcount = getRecordCount($srv_settings['table_prefix'].'results, '.$srv_settings['table_prefix'].'users, '.$srv_settings['table_prefix'].'tests, '.$srv_settings['table_prefix'].'gscales_grades', $i_sql_where_addon."".$srv_settings['table_prefix']."results.userid=".$srv_settings['table_prefix']."users.userid AND ".$srv_settings['table_prefix']."results.testid=".$srv_settings['table_prefix']."tests.testid AND ".$srv_settings['table_prefix']."results.gscaleid=".$srv_settings['table_prefix']."gscales_grades.gscaleid AND ".$srv_settings['table_prefix']."results.gscale_gradeid=".$srv_settings['table_prefix']."gscales_grades.gscale_gradeid");
$i_pageno = isset($_GET["pageno"]) ? (int)$_GET["pageno"] : 1;
if($i_pageno < 1)
 $i_pageno = 1;
$i_limitfrom = ($i_pageno-1)*$i_limitcount;
$i_pageno_count = floor(($i_recordcount - 1) / $i_limitcount) + 1;
if($i_limitfrom > $i_recordcount) {
 $i_pageno = $i_pageno_count;
$i_limitfrom = ($i_pageno-1)*$i_limitcount;
}
$i_url_limitto_addon .= "&limitto=".$i_limitcount;
$i_url_pageno_addon .= "&pageno=".$i_pageno;
$i_url_limit_addon .= $i_url_limitto_addon.$i_url_pageno_addon;
} else {
	$i_url_limitto_addon = "&limitto=";
$i_url_limit_addon .= $i_url_limitto_addon;
$i_limitfrom = -1;
$i_limitcount = -1;
} 
echo '<p><form name=resultsForm class=iactive method=post><table cellpadding=0 cellspacing=0 border=0 width="100%"><tr><td>';
echo '<table cellpadding=0 cellspacing=0 border=0 width="100%" style="background: url(images/toolbar-background.gif) repeat-x"><tr vAlign=center><td width=2><img src="images/toolbar-left.gif" width=2 height=32></td><td width=32>';
if($G_SESSION['access_reportsmanager'] > 1)
 echo '<img src="images/button-cross-big.gif" border=0 title="'.$lngstr['label_action_results_delete'].'" style="cursor: hand;" onclick="f=document.resultsForm;if (confirm(\''.$lngstr['qst_delete_records'].'\')) { f.action=\'reports-manager.php?action=delete&confirmed=1\';f.submit();}">';
else echo '<img src="images/button-cross-big-inactive.gif" border=0>';
echo '</td>';
if($G_SESSION['access_reportsmanager'] > 1) {
	echo '<td width=3><img src="images/toolbar-separator.gif" width=3 height=32 border=0></td><td width=3><img src="images/1x1.gif" width=3 height=1></td><td width=32>';
echo '<select name=userid onchange="document.location.href=\'reports-manager.php?userid=\'+this.value+\''.$i_testid_addon.$i_order_addon.$i_url_limit_addon.'\';">';
echo '<option value=""></option>';
$i_rSet2 = $g_db->Execute("SELECT * FROM ".$srv_settings['table_prefix']."users");
if(!$i_rSet2) {
 showDBError(__FILE__, 2);
} else {
 while(!$i_rSet2->EOF) {
 echo "<option value=".$i_rSet2->fields["userid"];
if(isset($_GET["userid"]) && ($_GET["userid"]==$i_rSet2->fields["userid"]))
 echo " selected=selected";
echo ">".convertTextValue($i_rSet2->fields["user_name"])."</option>\n";
$i_rSet2->MoveNext();
}
$i_rSet2->Close();
}
echo '</select>';
echo '</td><td width=3><img src="images/1x1.gif" width=3 height=1></td>';
}
echo '<td width=3><img src="images/toolbar-separator.gif" width=3 height=32 border=0></td><td width=3><img src="images/1x1.gif" width=3 height=1></td><td width=32>';
echo '<select name=testid onchange="document.location.href=\'reports-manager.php?testid=\'+this.value+\''.$i_userid_addon.$i_order_addon.$i_url_limit_addon.'\';">';
echo '<option value=""></option>';
$i_rSet2 = $g_db->Execute("SELECT * FROM ".$srv_settings['table_prefix']."tests");
if(!$i_rSet2) {
	showDBError(__FILE__, 2);
} else {
	while(!$i_rSet2->EOF) {
 echo "<option value=".$i_rSet2->fields["testid"];
if(isset($_GET["testid"]) && ($_GET["testid"]==$i_rSet2->fields["testid"]))
 echo " selected=selected";
echo ">".convertTextValue($i_rSet2->fields["test_name"])."</option>\n";
$i_rSet2->MoveNext();
}
$i_rSet2->Close();
}
echo '</select></td>';
echo '<td width="100%">&nbsp;</td>';
if($i_limitcount > 0) {
	if($i_pageno > 1) {
 echo '<td width=32><a href="reports-manager.php?pageno=1'.$i_url_limitto_addon.$i_userid_addon.$i_testid_addon.$i_order_addon.'"><img src="images/button-first-big.gif" border=0 title="'.$lngstr['button_first_page'].'"></a></td>';
echo '<td width=32><a href="reports-manager.php?pageno='.max(($i_pageno-1), 1).$i_url_limitto_addon.$i_userid_addon.$i_testid_addon.$i_order_addon.'"><img src="images/button-prev-big.gif" border=0 title="'.$lngstr['button_prev_page'].'"></a></td>';
} else {
 echo '<td width=32><img src="images/button-first-big-inactive.gif" border=0 title="'.$lngstr['button_first_page'].'"></td>';
echo '<td width=32><img src="images/button-prev-big-inactive.gif" border=0 title="'.$lngstr['button_prev_page'].'"></td>';
}
if($i_pageno < $i_pageno_count) {
 echo '<td width=32><a href="reports-manager.php?pageno='.min(($i_pageno+1), $i_pageno_count).$i_url_limitto_addon.$i_userid_addon.$i_testid_addon.$i_order_addon.'"><img src="images/button-next-big.gif" border=0 title="'.$lngstr['button_next_page'].'"></a></td>';
echo '<td width=32><a href="reports-manager.php?pageno='.$i_pageno_count.$i_url_limitto_addon.$i_userid_addon.$i_testid_addon.$i_order_addon.'"><img src="images/button-last-big.gif" border=0 title="'.$lngstr['button_last_page'].'"></a></td>';
} else {
 echo '<td width=32><img src="images/button-next-big-inactive.gif" border=0 title="'.$lngstr['button_next_page'].'"></td>';
echo '<td width=32><img src="images/button-last-big-inactive.gif" border=0 title="'.$lngstr['button_last_page'].'"></td>';
}
}
echo '<td width=2><img src="images/toolbar-right.gif" width=2 height=32></td></tr></table>';
echo '</td></tr><tr><td>';
echo '<table class=rowtable2 cellpadding=5 cellspacing=1 border=0 width="100%">';
echo '<tr><td class=rowhdr1 title="'.$lngstr['label_hdr_select_hint'].'" width=22><input type=checkbox name=toggleAll onclick="toggleCBs(this);"></td>';
writeQryTableHeaders('reports-manager.php?action='.$i_userid_addon.$i_testid_addon.$i_url_limit_addon, $i_tablefields, $i_order_no, $i_direction);
echo '<td class=rowhdr1 colspan=2>'.$lngstr['label_hdr_action'].'</td></tr>';
$i_rSet1 = $g_db->SelectLimit("SELECT ".$srv_settings['table_prefix']."results.resultid, ".$srv_settings['table_prefix']."results.result_datestart, ".$srv_settings['table_prefix']."results.userid, ".$srv_settings['table_prefix']."users.user_name, ".$srv_settings['table_prefix']."results.testid, ".$srv_settings['table_prefix']."tests.test_name, ".$srv_settings['table_prefix']."tests.test_attempts, ".$srv_settings['table_prefix']."results.result_timeexceeded, ".$srv_settings['table_prefix']."results.result_points, ".$srv_settings['table_prefix']."results.result_pointsmax, ROUND((".$srv_settings['table_prefix']."results.result_points/".$srv_settings['table_prefix']."results.result_pointsmax)*100, 0) as result_score, ".$srv_settings['table_prefix']."results.gscaleid, ".$srv_settings['table_prefix']."gscales_grades.grade_name, ".$srv_settings['table_prefix']."gscales_grades.grade_description FROM ".$srv_settings['table_prefix']."results, ".$srv_settings['table_prefix']."users, ".$srv_settings['table_prefix']."tests, ".$srv_settings['table_prefix']."gscales_grades WHERE ".$i_sql_where_addon."".$srv_settings['table_prefix']."results.userid=".$srv_settings['table_prefix']."users.userid AND ".$srv_settings['table_prefix']."results.testid=".$srv_settings['table_prefix']."tests.testid AND ".$srv_settings['table_prefix']."results.gscaleid=".$srv_settings['table_prefix']."gscales_grades.gscaleid AND ".$srv_settings['table_prefix']."results.gscale_gradeid=".$srv_settings['table_prefix']."gscales_grades.gscale_gradeid".$i_sql_order_addon, $i_limitcount, $i_limitfrom);
if(!$i_rSet1) {
	showDBError(__FILE__, 1);
} else {
	$i_counter = 0;
while(!$i_rSet1->EOF) {
 $rowname = ($i_counter % 2) ? "rowone" : "rowtwo";
echo '<tr id=tr_'.$i_pagewide_id.' class='.$rowname.' onmouseover="rollTR('.$i_pagewide_id.',1);" onmouseout="rollTR('.$i_pagewide_id.',0);"><td align=center width=22><input id=cb_'.$i_pagewide_id.' type=checkbox name=box_results[] value="'.$i_rSet1->fields["resultid"].'" onclick="toggleCB(this);"></td><td align=right>'.$i_rSet1->fields["resultid"].'</td><td>'.date($lngstr['date_format'],$i_rSet1->fields["result_datestart"]).'</td><td><a href="reports-manager.php?userid='.(isset($_GET["userid"]) && $_GET["userid"] != "" ? "" : $i_rSet1->fields["userid"]).$i_testid_addon.$i_order_addon.$i_url_limit_addon.'">'.convertTextValue($i_rSet1->fields["user_name"]).'</a></td><td><a href="reports-manager.php?testid='.(isset($_GET["testid"]) && $_GET["testid"] != "" ? "" : $i_rSet1->fields["testid"]).$i_userid_addon.$i_order_addon.$i_url_limit_addon.'">'.convertTextValue($i_rSet1->fields["test_name"]).'</a></td>';
 
 if($i_rSet1->fields["test_attempts"]) { 
 $i_attempt_count = 0;
$i_rSet4 = $g_db->Execute("SELECT test_attempt_count FROM ".$srv_settings['table_prefix']."tests_attempts WHERE testid=".$i_rSet1->fields["testid"]." AND userid=".$G_SESSION['userid']);
if(!$i_rSet4) {
 showDBError(__FILE__, 3);
} else {
 if(!$i_rSet4->EOF)
 $i_attempt_count = $i_rSet4->fields["test_attempt_count"];
$i_rSet4->Close();
}
echo '<td align=center>'.(($i_rSet1->fields["test_attempts"] <= $i_attempt_count) ? '<a href="reports-manager.php?action=attempts&set=0&testid='.$i_rSet1->fields["testid"].'&userid='.$i_rSet1->fields["userid"].$i_order_addon.$i_url_limit_addon.'"><img src="images/button-checkbox-2.gif" width=13 height=13 border=0 title="'.$lngstr['label_yes'].'"></a>' : '<a href="reports-manager.php?action=attempts&set=1&testid='.$i_rSet1->fields["testid"].'&userid='.$i_rSet1->fields["userid"].$i_order_addon.$i_url_limit_addon.'"><img src="images/button-checkbox-0.gif" width=13 height=13 border=0 title="'.$lngstr['label_no'].'"></a>').'</td>';
} else {
 echo '<td align=center><img src="images/button-checkbox-0.gif" width=13 height=13 border=0 title="'.$lngstr['label_no'].'"></td>';
}
echo '<td align=center>'.($i_rSet1->fields["result_timeexceeded"] ? '<img src="images/button-checkbox-2.gif" width=13 height=13 border=0 title="'.$lngstr['label_yes'].'">' : '<img src="images/button-checkbox-0.gif" width=13 height=13 border=0 title="'.$lngstr['label_no'].'">').'</td><td align=right>'.$i_rSet1->fields["result_points"].'</td><td align=right>'.$i_rSet1->fields["result_pointsmax"].'</td><td align=right>'.sprintf("%.1f", $i_rSet1->fields["result_score"]).'%</td><td align=center title="'.convertTextValue($i_rSet1->fields["grade_description"]).'">'.convertTextValue($i_rSet1->fields["grade_name"]).'</td>';
echo '<td align=center width=22><a href="reports-manager.php?resultid='.$i_rSet1->fields["resultid"].'&action=viewq"><img width=20 height=20 border=0 src="images/button-view.gif" title="'.$lngstr['label_action_test_result_view'].'"></a></td>';
if($G_SESSION['access_reportsmanager'] > 1)
 echo '<td align=center width=22><a href="reports-manager.php?resultid='.$i_rSet1->fields["resultid"].'&action=delete" onclick="return confirmMessage(this, \''.$lngstr['qst_delete_record'].'\')"><img width=20 height=20 border=0 src="images/button-cross.gif" title="'.$lngstr['label_action_test_result_delete'].'"></a></td>';
echo '</tr>';
$i_counter++;
$i_pagewide_id++;
$i_rSet1->MoveNext();
}
$i_rSet1->Close();
}
echo '</table>';
echo '</td></tr></table></form>';
require_once($DOCUMENT_INC."btm.inc.php");
?>

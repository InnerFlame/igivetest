<?php
require_once($DOCUMENT_INC."top.inc.php");
$f_subjectid = (int)readGetVar('subjectid');
writePanel2(array(
	array(0 => '<a class=bar2 href="question-bank.php">'.$lngstr['page_header_questionbank'].'</a>', 0),
	array(0 => '<a class=bar2 href="subjects.php">'.$lngstr['page_header_subjects'].'</a>', 0)
));
echo '<h2>'.$lngstr['page_header_subjects_settings'].'</h2>';
writeErrorsWarningsBar();
 
$i_rSet1 = $g_db->Execute("SELECT * FROM ".$srv_settings['table_prefix']."subjects WHERE subjectid=$f_subjectid");
if(!$i_rSet1) {
	showDBError(__FILE__, 1);
} else {
	if(!$i_rSet1->EOF) {
 echo '<p><form method=post action="subjects.php?subjectid='.$f_subjectid.'&action=edit">';
echo '<table class=rowtable2 cellpadding=5 cellspacing=1 border=0 width="100%">';
echo '<tr class=rowtwo valign=top><td>'.$lngstr['page_subjects_subjectid'].'</td><td>'.$i_rSet1->fields["subjectid"].'</td></tr>';
echo '<tr class=rowone valign=top><td>'.$lngstr['page_subjects_subjectname'].'</td><td>'.getInputElement('subject_name', $i_rSet1->fields["subject_name"]).'</td></tr>';
echo '<tr class=rowtwo valign=top><td>'.$lngstr['page_subjects_subjectdescription'].'</td><td>'.getTextArea('subject_description', $i_rSet1->fields["subject_description"]).'</td></tr>';
echo '</table>'; 
 echo '<p class=center><input class=btn type=submit name=bsubmit value=" '.$lngstr['button_update'].' "> <input class=btn type=submit name=bcancel value=" '.$lngstr['button_cancel'].' "></form>';
}
$i_rSet1->Close();
}
require_once($DOCUMENT_INC."btm.inc.php");
?>

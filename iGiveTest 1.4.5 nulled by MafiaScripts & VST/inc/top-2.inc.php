<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="<?php echo $lngstr['text_direction']; ?>"><head><title><?php echo $page_title.$lngstr['item_separator'].$srv_settings['title_postfix'] ?></title>
<meta http-equiv="Content-Language" content="<?php echo $lngstr['meta_contentlanguage']; ?>">
<meta content="text/html; charset=<?php echo $lngstr['meta_charset']; ?>" http-equiv=Content-Type>
<link rel="SHORTCUT ICON" href="/favicon.ico">
<link href="shared/style.css" rel=stylesheet type=text/css>
<script language=javascript src="shared/shared.js" type="text/javascript"></script>
<?php echo $page_meta; ?></head>
<body bgcolor="#ffffff"<?php echo $page_body_tag; ?>>
<p><table cellpadding=0 cellspacing=0 border=0 width="100%">
<tr><td width=200><?php echo $lngstr['branding']['top_logo']; ?></td><td width="100%" align=center class=top_section><?php echo $page_title; ?></td></tr>
<tr><td colspan=2 height=1 width=100% style="background-color: #E7E9EF;"><img src="images/1x1.gif" width=1 height=1></td></tr><?php
if(empty($g_nocontrolpanel)) {
	echo '<tr><td colspan=2>';
echo '<table cellpadding=0 cellspacing=0 border=0 width="100%" style="border-left: 1px solid #E0E7F6; border-right: 1px solid #CFD6E3; border-bottom: 1px solid #CFD6E3;">';
echo '<tr style="background: url(images/mainbar-background.gif) repeat-x;">';
echo '<td width=1 style="background: #ffffff"><img src="images/1x1.gif" width=1 height=32></td><td align=center><b><a class=bar1 href="index.php">'.$lngstr['panel_home'].'</a></b></td>';
if($G_SESSION['access_questionbank'] > 0)
 echo '<td align=center><b><a class=bar1 href="question-bank.php">'.$lngstr['panel_questionbank'].'</a></b></td>';
else if($G_SESSION['access_subjects'] > 0)
 echo '<td align=center><b><a class=bar1 href="subjects.php">'.$lngstr['panel_questionbank'].'</a></b></td>';
if($G_SESSION['access_testmanager'] > 0)
 echo '<td align=center><b><a class=bar1 href="test-manager.php">'.$lngstr['panel_edittests'].'</a></b></td>';
else if($G_SESSION['access_gradingsystems'] > 0)
 echo '<td align=center><b><a class=bar1 href="grades.php">'.$lngstr['panel_edittests'].'</a></b></td>';
else if($G_SESSION['access_emailtemplates'] > 0)
 echo '<td align=center><b><a class=bar1 href="email-templates.php">'.$lngstr['panel_edittests'].'</a></b></td>';
else if($G_SESSION['access_reporttemplates'] > 0)
 echo '<td align=center><b><a class=bar1 href="report-templates.php">'.$lngstr['panel_edittests'].'</a></b></td>';
if($G_SESSION['access_reportsmanager'] > 0)
 echo '<td align=center><b><a class=bar1 href="reports-manager.php">'.$lngstr['panel_results'].'</a></b></td>';
if($G_SESSION['access_users'] > 0)
 echo '<td align=center><b><a class=bar1 href="users.php">'.$lngstr['panel_usersandgroups'].'</a></b></td>';
else if($G_SESSION['access_groups'] > 0)
 echo '<td align=center><b><a class=bar1 href="groups.php">'.$lngstr['panel_usersandgroups'].'</a></b></td>';
else if($G_SESSION['access_config'] > 0)
 echo '<td align=center><b><a class=bar1 href="config.php">'.$lngstr['panel_usersandgroups'].'</a></b></td>';
else if($G_SESSION['access_visitors'] > 0)
 echo '<td align=center><b><a class=bar1 href="visitors.php">'.$lngstr['panel_usersandgroups'].'</a></b></td>';
echo '<td align=center><b><a class=bar1 href="signout.php">'.$lngstr['panel_signout'].'</a></b></td>';
echo '<td width=1 style="background: #ffffff"><img src="images/1x1.gif" width=1 height=32></td></tr></table>';
echo '</td></tr>';
}
?>
<tr><td colspan=2 height=7 width=100%><img src="images/1x1.gif" width=1 height=7></td></tr>
<tr><td colspan=2>
<table cellpadding=0 cellspacing=0 border=0 width="100%">
<tr vAlign=top><td width="1%"><img src="images/1x1.gif" width=2 height=1></td><td>
<table cellpadding=0 cellspacing=0 border=0 width="100%">
<tr><td class=box_area>

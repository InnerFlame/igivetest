<?php
require_once("const.inc.php");
require_once("config.inc.php");
 
session_name(SYSTEM_SESSION_ID);
session_start();
session_register('MAIN');
if(!isset($_SESSION['MAIN']))
 $_SESSION['MAIN'] = array();
$G_SESSION = &$_SESSION['MAIN'];
$DOCUMENT_ROOT = $srv_settings['dir_root_full'];
$DOCUMENT_INC = $DOCUMENT_ROOT."inc/";
$DOCUMENT_LANG = $DOCUMENT_INC."languages/";
$DOCUMENT_PAGES = $DOCUMENT_ROOT."inc/pages/";
$DOCUMENT_FPDF = $DOCUMENT_ROOT."inc/fpdf/";
require_once($DOCUMENT_LANG.$srv_settings['language'].".lng.php");
require_once($DOCUMENT_INC."functions.inc.php");
require_once($DOCUMENT_INC."adodb/adodb.inc.php");
require_once($DOCUMENT_INC."connect.inc.php");
require_once($DOCUMENT_INC."logs.inc.php");
 
if(!isset($G_SESSION['config_itemsperpage'])) {
	$G_SESSION['config_itemsperpage'] = getConfigItem(CONFIG_list_length);
if(!$G_SESSION['config_itemsperpage'])
 $G_SESSION['config_itemsperpage'] = 30;
}
if(!isset($G_SESSION['config_editortype'])) {
	$G_SESSION['config_editortype'] = getConfigItem(CONFIG_editor_type);
if(!$G_SESSION['config_editortype'])
 $G_SESSION['config_editortype'] = 2;
}
 
$input_err_msg = "";
$input_inf_msg = "";
$page_title = "";
$page_meta = "";
$page_body_tag = "";
?>

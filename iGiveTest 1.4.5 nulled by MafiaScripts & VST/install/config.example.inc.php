<?php // Config file (example):

$srv_settings['title_postfix'] = 'VST';
$srv_settings['default_email'] = '';
$srv_settings['language'] = 'en';

$srv_settings['db_driver'] = 'mysql'; /* Database Type */
$srv_settings['db_host'] = 'localhost'; /* Database Host */
$srv_settings['db_db'] = ''; /* Database Name */
$srv_settings['db_user'] = ''; /* Database User Name*/
$srv_settings['db_password'] = ''; /* Database Password */
$srv_settings['table_prefix'] = '';

$srv_settings['url_root'] = '/';
$srv_settings['dir_root_full'] = '/var/www/html/';
$srv_settings['url_files'] = '/files/';
$srv_settings['dir_files_full'] = '/var/www/html/files/';

?>
<?php
require_once("inc/init.inc.php");
if(isset($G_SESSION['userid'])) {
	if($G_SESSION['access_users'] > 0) { 
 $page_title = $lngstr['page_title_manageusers'];
if(isset($_GET['action'])) {
 switch($_GET['action']) {
 case 'create': 
 if($G_SESSION['access_users'] > 1) { 
 include_once($DOCUMENT_PAGES."manageusers-4.inc.php");
} else {
 gotoLocation('users.php'.getURLAddon('', array('action')));
}
break;
case 'delete': 
 if($G_SESSION['access_users'] > 1) {
 $f_confirmed = readGetVar('confirmed'); 
 if($f_confirmed==1) {
 if(isset($_GET['userid']) || isset($_POST["box_users"])) { 
 include_once($DOCUMENT_PAGES."manageusers-5.inc.php");
} else {
 gotoLocation('users.php');
}
} else if($f_confirmed=='0') {
 gotoLocation('users.php');
} else { 
 $i_confirm_header = $lngstr['page_manageusers_delete_user'];
$i_confirm_request = $lngstr['qst_delete_user'];
$i_confirm_url = 'users.php?userid='.(int)$_GET['userid'].'&action=delete';
include_once($DOCUMENT_PAGES."confirm.inc.php");
}
} else {
 gotoLocation('users.php'.getURLAddon('', array('action', 'confirmed')));
}
break;
case 'edit': 
 $page_title = $lngstr['page_title_users_settings'].$lngstr['item_separator'].$page_title;
if(isset($_GET['userid'])) {
 if(isset($_POST['bsubmit'])) {
 if($G_SESSION['access_users'] > 1) {
 include_once($DOCUMENT_PAGES."manageusers-3.inc.php");
} else {
 gotoLocation('users.php'.getURLAddon('', array('action')));
}
} else if(isset($_POST['bcancel'])) {
 gotoLocation('users.php');
} else {
 include_once($DOCUMENT_PAGES."manageusers-2.inc.php");
}
}
break;
case 'enable': 
 if($G_SESSION['access_users'] > 1) {
 if(isset($_GET['userid'])) {
 include_once($DOCUMENT_PAGES."manageusers-6.inc.php");
}
} else {
 gotoLocation('users.php'.getURLAddon('', array('action', 'set')));
}
break;
case 'notes': 
 if(isset($_GET['userid'])) {
 include_once($DOCUMENT_PAGES."manageusers-7.inc.php");
}
break;
case 'groups': 
 $page_title = $lngstr['page_title_users_memberof'].$lngstr['item_separator'].$page_title;
if(isset($_GET['userid']) || isset($_POST["box_users"]) || isset($_GET["userids"])) {
 include_once($DOCUMENT_PAGES."manageusers-8.inc.php");
} else {
 gotoLocation('users.php');
}
break;
case 'memberof': 
 if($G_SESSION['access_users'] > 1) {
 if(isset($_GET['groupid']) && isset($_GET["userids"])) {
 include_once($DOCUMENT_PAGES."manageusers-9.inc.php");
}
} else {
 gotoLocation('users.php'.getURLAddon('?action=groups', array('action')));
}
break;
default:
 include_once($DOCUMENT_PAGES."manageusers-1.inc.php");
}
} else {
 include_once($DOCUMENT_PAGES."manageusers-1.inc.php");
}
} else { 
 $input_inf_msg = $lngstr['inf_cant_access_users'];
include_once($DOCUMENT_PAGES."home.inc.php");
}
} else { 
	$page_title = $lngstr['page_title_signin'];
include_once($DOCUMENT_PAGES."signin-1.inc.php");
}
?>
